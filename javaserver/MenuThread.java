package application;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import application.Invoice.Product;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MenuThread extends Thread{
    /**
     * Command line menu to give the user options about what task to perform
     */

    public void run(){

        while(true){
            System.out.println("Enter'R' to generate report, the total of all invoices saved");
            System.out.println("Enter 'D' to delete all invoices from the server");
            System.out.println("Enter 'E' to terminate the server");

            Scanner scanner = new Scanner(System.in);
            System.out.println("\nEnter the task to perform");
            String task = scanner.nextLine().toUpperCase();

            switch (task) {
                case "R":
                    System.out.println(report());
                    break;
                case "D":
                    delete();
                    break;
                case "E":
                    System.exit(0);
                default:
                    System.out.println("Invalid Task\n");
            }

        }
    }

    /**
     * Generate a report about the invoice files on the server
     * @return details of the invoices and products
     */

    private String report() {
        ArrayList<Product> products = new ArrayList<Product>();
        double total = 0;
        Invoice invoice = new Invoice(products,total);
        List<Invoice> invoices = new ArrayList<Invoice>();

        String name = null;
        double price = 0;

        Product product = new Product(name, price);

       /* List<Product> products = new ArrayList<Product>();
        Invoice.Product product = new Invoice.Product(name, price);

        */

        System.out.println("REPORT\n");

        String path = System.getProperty("user.home") + File.separator + "Invoices";

        File directory = new File(path);
        File fileList[] = directory.listFiles();

        for (File file : fileList) {
            String readThisFile = file.getName().toLowerCase();

            if(readThisFile.endsWith(".json")){
                try{
                // create a Gson instance
                Gson gson = new Gson();

                // create a readder
                URI uri = file.toURI();
                Reader reader = Files.newBufferedReader(Paths.get(uri));

                // convert JSON string to object
                JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);

                    reader.close();

                    products = new ArrayList<Product>();

                    JsonArray jsonArray = jsonObject.get("invoice").getAsJsonObject().get("products").getAsJsonObject().get("product").getAsJsonArray();

                    for(JsonElement element: jsonArray){
                        JsonObject elementAsJsonObject = element.getAsJsonObject();
                        name = elementAsJsonObject.get("name").getAsString();
                        price = elementAsJsonObject.get("price").getAsDouble();

                        product = new Product(name, price);
                        products.add(product);

                    }

                    total = 0;
                    for(Product p: products){
                        System.out.println(p.toString());
                        total += p.getPrice();
                    }

                    invoice = new Invoice(products,total);
                    invoices.add(invoice);
                    System.out.println(invoice.toString());




                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if(readThisFile.endsWith(".xml")){

                try {
                    // make an XML document
                    Document xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);

                    // search the xml for occurrences of name
                    NodeList nodeList = xml.getElementsByTagName("name");
                    String nameArray[] = new String[nodeList.getLength()];
                    for(int i =0; i< nodeList.getLength(); i++){
                        name = nodeList.item(i).getTextContent();
                        nameArray[i] = name;

                    }
                    // search the xml for occurrences of price
                    nodeList = xml.getElementsByTagName("price");
                    double priceArray[] = new double[nodeList.getLength()];
                    for(int i =0; i< nodeList.getLength(); i++){
                        price = Double.parseDouble(nodeList.item(i).getTextContent());
                        priceArray[i] = price;

                    }

                    products = new ArrayList<Product>();
                    for(int i =0; i< nodeList.getLength(); i++){
                        product = new Product(nameArray[i], priceArray[i]);
                        products.add(product);

                    }

                    total = 0;
                    for(Product p: products){
                        System.out.println(p.toString());
                        total += p.getPrice();
                    }

                    invoice = new Invoice(products,total);
                    invoices.add(invoice);
                    System.out.println(invoice.toString());



                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                }

            }

        }
        total = 0;
        int invoiceCount = 0;
        for(Invoice i: invoices){
            total += i.getTotal();
            invoiceCount ++;
        }
        return "The total amount for "+ invoices.size() + " invoice is $" + String.format("%.2f", total)+ "\n";
    }

    /**
     * Displays a list of all invoices (XML and JSON files) from Invoices folder
     * Delete all invoices (XML and JSON files) from Invoices folder
     */

    private void delete() {

        String path = System.getProperty("user.home") + File.separator + "Invoices";

        System.out.println("WARNING! YOU HAVE CHOSEN TO DELETE ALL INVOICES.\n" + "THIS WILL DELETE ALL FILES ENDING WITH .xml OR .json FROM " + path);
        System.out.println("THE FOLLOWING FILES WILL BE DELETED: \n");

        File directory = new File(path);
        File fileList[] = directory.listFiles();

        for (File file : fileList) {
            String fileToDelete = file.getName().toLowerCase();
            if (fileToDelete.endsWith(".xml") || fileToDelete.endsWith(".json")) {
                System.out.println(fileToDelete);
            }
        }

        System.out.println("TYPE 'DELETE' AND PRESS ENTER TO PROCEED OR PRESS ENTER TO CANCEL");

        Scanner scanner = new Scanner(System.in);
        String delete = scanner.nextLine().toUpperCase();
        if (delete.equals("DELETE")) {
            for (File file : fileList) {
                String fileToDelete = file.getName().toLowerCase();
                if (fileToDelete.endsWith(".xml") || fileToDelete.endsWith(".json")) {
                    file.delete();
                }

            }
            System.out.println("All invoices delete\n");

        } else {
            System.out.println("Operation cancelled. No invoices have been deleted\n");
        }
    }
}
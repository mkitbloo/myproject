package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    /**
     * Program begins here
     * Create and display the Address scene which users can type server address
     * @param args
     */


    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception{

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddressScene.fxml"));
        Parent root = (Parent) fxmlLoader.load();

        // open the login scene

         stage.setTitle("Server Address");
         stage.setScene(new Scene(root));
         stage.show();
    }


}

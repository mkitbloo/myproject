package application;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Cosmetics {

    //Attributes or state
    private StringProperty itemId;
    private StringProperty userId;
    private StringProperty label;
    private StringProperty brand;

    //Constructor
    public Cosmetics(String itemId,String userId, String label, String brand){
        this.itemId = new SimpleStringProperty(itemId);
        this.userId = new SimpleStringProperty(userId);
        this.label = new SimpleStringProperty(label);
        this.brand = new SimpleStringProperty(brand);
    }

    //Methods or Behavior (Setter-Mutator, Getter-Accessor)
    public void setItemId(String itemId){
        this.itemId.set(itemId);
    }
    public String getItemId(){
        return itemId.get();
    }
    public StringProperty itemIdProperty(){
        return itemId;
    }

    //Methods or Behavior (Setter-Mutator, Getter-Accessor)
    public void setUserId(String userId){
        this.userId.set(userId);
    }
    public String getUserId(){
        return userId.get();
    }
    public StringProperty userIdProperty(){
        return userId;
    }

    //Methods or Behavior (Setter-Mutator, Getter-Accessor)
    public void setLabel(String label){
        this.label.set(label);
    }
    public String getLabel(){
        return label.get();
    }
    public StringProperty labelProperty(){
        return label;
    }

    //Methods or Behavior (Setter-Mutator, Getter-Accessor)
    public void setBrand(String brand){
        this.brand.set(brand);
    }
    public String getBrand(){
        return brand.get();
    }
    public StringProperty brandProperty(){
        return brand;
    }

}

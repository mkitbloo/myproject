package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class LoginController {

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Text errorMessage;

    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;


    /**
     * Takes the user to the AccountScene
     * Closes LoginScene
     * Opens AccountScene
     * @param actionEvent
     */

    @FXML
    protected void createAccountButtonAction(ActionEvent actionEvent){
        try {
            // close the login scene
            Node node = (Node) actionEvent.getSource();
            Stage loginStage = (Stage) node.getScene().getWindow();
            loginStage.close();

            FXMLLoader fxmlLoader =
                    new FXMLLoader(getClass().getResource("AccountScene.fxml"));
            Parent root = (Parent) fxmlLoader.load();


            // open the account scene
            Stage stage = new Stage();
            stage.setTitle("Create Account");
            stage.setScene(new Scene(root));
            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Can't open Account scene");
            errorMessage.setText("Can't open Account window");
        }
    }

    /**
     * Verify the login credentials
     * Close the LoginScene
     * Open the DatabaseScene
     * @param actionEvent
     */

   public void loginButtonAction(ActionEvent actionEvent){
        // Build a user from database results
        User user = getUser();

        System.out.println(usernameField.getText());
        System.out.println(passwordField.getText());

        if(usernameField.getText().equals(user.getUsername())
            &&(!usernameField.getText().equals(""))
               &&(checkPassword(user) == true)){

                try{
                    //close the login scene
                    Node node = (Node) actionEvent.getSource();
                    Stage loginStage = (Stage) node.getScene().getWindow();
                    loginStage.close();

                    FXMLLoader fxmlLoader =
                            new FXMLLoader(getClass().getResource("DatabaseScene.fxml"));
                    Parent root = (Parent) fxmlLoader.load();

                    // Send the DatabaseController the user id of the user that is logged in
                    // this will be the FOREIGN KEY for the pets table
                    DatabaseController databaseController = fxmlLoader.<DatabaseController>getController();
                    databaseController.setFK(user.getUserId());


                    // open the database scene
                    Stage stage = new Stage();
                    stage.setTitle("Cosmetics Database");
                    stage.setScene(new Scene(root));
                    stage.show();

                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Can't open Cosmetics Database scene");
                    errorMessage.setText("Can't open Cosmetics Database window");
                }
       } else {
            errorMessage.setText("Invalid username or password");
       }
   }


    /**
     * Get the user info from the database
     * @return
     */
    private User getUser() {

        User user = new User();

        try {
            connection = DatabaseConnector.connect();
            preparedStatement =
                    connection.prepareStatement("SELECT * FROM accounts WHERE username = ?");
            preparedStatement.setString(1, usernameField.getText());
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                user.setUserId(resultSet.getString(1));
                user.setUsername(resultSet.getString(2));
                user.setPassword(resultSet.getString(3));
            }
            resultSet.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            errorMessage.setText("Can't get user from database");
        }
        return user;
    }

    private Boolean checkPassword(User user){


        if(BCrypt.checkpw(passwordField.getText(), user.getPassword())){
            // it matches
            System.out.println("Password matches");
            return true;
        } else {
            // it doesn't match
            System.out.println("Password doesn't match");
            return false;
        }
    }
}

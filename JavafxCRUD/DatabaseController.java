package application;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseController {

    @FXML
    private TextField searchTextField;

    @FXML
    private ToggleGroup toggleGroup;
    @FXML
    private RadioButton insertIntoRadioBtn;
    @FXML
    private RadioButton updateRadioBtn;
    @FXML
    private RadioButton deleteRadioBtn;

    @FXML
    private TableView<Cosmetics> tableView;
    @FXML
    private TableColumn<Cosmetics, String> itemIdColumn;
    @FXML
    private TableColumn<Cosmetics, String> userIdColumn;
    @FXML
    private TableColumn<Cosmetics, String> labelColumn;
    @FXML
    private TableColumn<Cosmetics, String> brandColumn;

    @FXML
    private TextField itemIdTextField;
    @FXML
    private TextField userIdTextField;
    @FXML
    private TextField labelTextField;
    @FXML
    private TextField brandTextField;

    @FXML
    private Button insertIntoBtn;
    @FXML
    private Button updateBtn;
    @FXML
    private Button deleteBtn;

    @FXML
    private Text errorMessage;

    //from LoginController, the PK of the user that is logged in
    private String userId;

    private static Connection connection;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    /**
     * This method should receive the User Id from the LoginController
     * We can use the User Id to identify records belonging to the user
     *
     * User id is the PK from accounts and the FK from cosmetics
     * @param userId
     */
    public void setFK(String userId){
        //from LoginController, the PK of the user that is logged in
        this.userId = userId;
    }

    /**
     * Sets up the table view and fills the table with the data from the database
     */

    @FXML
    private void initialize(){
        //initialize the tableView with four columns
        itemIdColumn.setCellValueFactory(cellData -> cellData.getValue().itemIdProperty());
        userIdColumn.setCellValueFactory(cellData -> cellData.getValue().userIdProperty());
        labelColumn.setCellValueFactory(cellData -> cellData.getValue().labelProperty());
        brandColumn.setCellValueFactory(cellData -> cellData.getValue().brandProperty());

        setVisibleItems();

        Platform.runLater(() -> {
            //fill the tableView with data from observableList
            try {
            System.out.println("User ID is: " + userId);
            tableView.setItems(getCosmeticsData());

            buildCosmetics();
            sortFilterTableView();
            observeRadioButtonChanges();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        });
    }

    /**
     * Observes which radio button is selected and shows and hides buttons and text inputs accordingly
     */
    private void observeRadioButtonChanges() {

        toggleGroup.selectedToggleProperty().addListener((observableValue, oldValue, newValue) -> {

            // Cast object to Radio Button
            RadioButton radioButton = (RadioButton)newValue.getToggleGroup().getSelectedToggle();


            if (radioButton.getText().contains("Insert Into")){

                insertIntoBtn.setVisible(true);
                insertIntoBtn.setManaged(true);

                updateBtn.setVisible(false);
                updateBtn.setManaged(false);

                deleteBtn.setVisible(false);
                deleteBtn.setManaged(false);

                labelTextField.setVisible(true);
                labelTextField.setManaged(true);

                brandTextField.setVisible(true);
                brandTextField.setManaged(true);

            }else if(radioButton.getText().contains("Update")){

                insertIntoBtn.setVisible(false);
                insertIntoBtn.setManaged(false);

                updateBtn.setVisible(true);
                updateBtn.setManaged(true);

                deleteBtn.setVisible(false);
                deleteBtn.setManaged(false);

                labelTextField.setVisible(true);
                labelTextField.setManaged(true);

                brandTextField.setVisible(true);
                brandTextField.setManaged(true);


            }else{

                insertIntoBtn.setVisible(false);
                insertIntoBtn.setManaged(false);

                updateBtn.setVisible(false);
                updateBtn.setManaged(false);

                deleteBtn.setVisible(true);
                deleteBtn.setManaged(true);

                labelTextField.setVisible(false);
                labelTextField.setManaged(false);

                brandTextField.setVisible(false);
                brandTextField.setManaged(false);


            }


        });



    }


    /**
     * Searchbox to search the table view results
     * @throws SQLException
     */




    private void sortFilterTableView() throws SQLException {

        FilteredList<Cosmetics> filteredList = new FilteredList<>(getCosmeticsData(), p -> true);


        searchTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(cosmetics -> {
                //if search text is empty display all cosmetics
                if(newValue == null || newValue.isEmpty()){
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (cosmetics.getLabel().toLowerCase().contains(lowerCaseFilter)){
                    return true; // search string is a match
                } else if(cosmetics.getBrand().toLowerCase().contains(lowerCaseFilter)){
                    return true; // search string is a match
                }
                return false; //does not match
            });
        });
    SortedList<Cosmetics> SortedList = new SortedList<>(filteredList);
    SortedList.comparatorProperty().bind(tableView.comparatorProperty());
    tableView.setItems(SortedList);

    }

    /**
     * Returns the list of cosmetics from the database
     *
     * @return the list of cosmetics from the database
     */

    private ObservableList<Cosmetics> getCosmeticsData() throws SQLException {

        connection = DatabaseConnector.connect();
        preparedStatement = connection.prepareStatement("SELECT * FROM cosmetics WHERE user_id=?");
        preparedStatement.setString(1,userId);
        resultSet = preparedStatement.executeQuery();

        ObservableList<Cosmetics> cosmeticsList = FXCollections.observableArrayList();

        while(resultSet.next()){
            String itemId = resultSet.getString(1);
            String userId = resultSet.getString(2);
            String label = resultSet.getString(3);
            String brand = resultSet.getString(4);

            //construct cosmetics objects
            Cosmetics cosmetics = new Cosmetics(itemId,userId,label,brand);

            //add the cosmetic to the list
            cosmeticsList.add(cosmetics);

        }
        DatabaseConnector.disconnect();;
        return cosmeticsList;
    }

    /**
     * Selects the Insert Into radio button and hides the Updates and Delete buttons
     */
    private void setVisibleItems() {

        Toggle toggle = toggleGroup.getSelectedToggle();
        System.out.println("Radio button selected: " + toggle);

        if(toggle == null){
            //set what the user sees and doesn't see
            insertIntoBtn.setVisible(true);
            insertIntoBtn.setManaged(true);

            updateBtn.setVisible(false);
            updateBtn.setManaged(false);

            deleteBtn.setVisible(false);
            deleteBtn.setManaged(false);

            insertIntoRadioBtn.setSelected(true);
        }


    }

    @FXML
    public void insertBtnAction(ActionEvent actionEvent) {

        if(labelTextField.getText() !=null && !labelTextField.getText().isEmpty()
        && brandTextField.getText() !=null && !brandTextField.getText().isEmpty()){

            Cosmetics cosmetics = new Cosmetics(null, null, null, null);

            cosmetics.setLabel(labelTextField.getText());
            cosmetics.setBrand(brandTextField.getText());

            try {
                PreparedStatement preparedStatement =
                        DatabaseConnector.connect().prepareStatement("INSERT INTO cosmetics VALUES(?,?,?,?)");
                preparedStatement.setString(1,null);
                preparedStatement.setString(2,userId);
                preparedStatement.setString(3,cosmetics.getLabel());
                preparedStatement.setString(4,cosmetics.getBrand());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                DatabaseConnector.disconnect();
                initialize();

                errorMessage.setText("");

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } else{
            errorMessage.setText("Please complete all fields");
        }
    }

    @FXML
    public void updateBtnAction(ActionEvent actionEvent) {

        if(labelTextField.getText() !=null && !labelTextField.getText().isEmpty()
                && brandTextField.getText() !=null && !brandTextField.getText().isEmpty()){

            Cosmetics cosmetics = new Cosmetics(null, null, null, null);

            cosmetics.setItemId(itemIdTextField.getText());
            cosmetics.setUserId(userIdTextField.getText());
            cosmetics.setLabel(labelTextField.getText());
            cosmetics.setBrand(brandTextField.getText());

            try {
                PreparedStatement preparedStatement =
                        DatabaseConnector.connect().prepareStatement("UPDATE cosmetics SET label=?,brand=? WHERE item_id=?");
                preparedStatement.setString(1,cosmetics.getLabel());
                preparedStatement.setString(2,cosmetics.getBrand());
                preparedStatement.setString(3,cosmetics.getItemId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                DatabaseConnector.disconnect();
                initialize();

                errorMessage.setText("");


            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } else{
            errorMessage.setText("Please complete all fields!!");
        }




    }
    @FXML
    public void deleteBtnAction(ActionEvent actionEvent) {


        if(itemIdTextField.getText() !=null && !labelTextField.getText().isEmpty())

        {

            Cosmetics cosmetics = new Cosmetics(null, null, null, null);

            cosmetics.setItemId(itemIdTextField.getText());
            cosmetics.setUserId(userIdTextField.getText());
            cosmetics.setLabel(labelTextField.getText());
            cosmetics.setBrand(brandTextField.getText());

            try {
                PreparedStatement preparedStatement =
                        DatabaseConnector.connect().prepareStatement("DELETE FROM cosmetics WHERE item_id=?");
                preparedStatement.setString(1,cosmetics.getItemId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                DatabaseConnector.disconnect();
                initialize();

                errorMessage.setText("");

                labelTextField.setText("");
                brandTextField.setText("");

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        } else{
            errorMessage.setText("Please select an item from the table!");
        }




    }

        /**
         * This method should build a pet when a table row is clicked
         * and pre fill the form
         */
        public void buildCosmetics(){
            tableView.setOnMouseClicked((MouseEvent mouseEvent) -> {
                if(mouseEvent.getClickCount()>0) {

                    //Cosmetics cosmetics = new Cosmetics(null,null,null,null);

                    if(tableView.getSelectionModel().getSelectedItem() !=null){
                        Cosmetics cosmetics = tableView.getSelectionModel().getSelectedItem();
                        itemIdTextField.setText(cosmetics.getItemId());
                        userIdTextField.setText(cosmetics.getUserId());
                        labelTextField.setText(cosmetics.getLabel());
                        brandTextField.setText(cosmetics.getBrand());
                    }}


            });

        }

    /**
     *     public void setFK(String userId) {
     *             //from LoginController, the PK of the user that is logged in
     *         this.userId = userId;
     *     }
      * @param userId
     */

}

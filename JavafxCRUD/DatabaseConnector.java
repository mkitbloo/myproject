package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {

    //public so AddressController can check if null or not
    public static Connection connection = null;

    /**
     * Connect to the database
     * @return
     */

    public static Connection connect(){
        try {
        // Server address from user input
        String address = AddressController.address;


            connection = DriverManager.getConnection(
                    "jdbc:mariadb://" + address +
                            ":3306/cosmetics_javafx?useLegacyDatetimeCode=false&serverTimezone=UTC",
                    "seon","RExRUHXKDnUUP1BY");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return connection;
    }

    /**
     * Disconnect from the database
     */
    public static void disconnect(){
        if(connection != null){
            try {
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
